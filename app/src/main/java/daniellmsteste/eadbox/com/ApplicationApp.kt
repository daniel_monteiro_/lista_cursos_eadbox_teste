package daniellmsteste.eadbox.com

import android.app.Application
import io.realm.Realm

/**
 * @author Dniel Monteiro
 *
 * @since on 11/07/2018.
 */

class ApplicationApp: Application() {

    override fun onCreate() {
        super.onCreate()
        Realm.init(this)
    }

}