package daniellmsteste.eadbox.com.presenter.view

import daniellmsteste.eadbox.com.model.ResponseCursos
import retrofit2.Response

/**
 * @author Daniel Monteiro
 *
 * @since on 09/07/2018.
 */

interface CursoPresenterView {

    interface ViewListaCursos {
        fun showProgressLoading()
        fun hideProgressLoading()
        fun onSuccess(responseCursos: Response<ArrayList<ResponseCursos>>)
        fun onError(throwable: Throwable)
        fun validateError()
        fun checkInternet(): Boolean
    }

    interface CursosPresenter {
        fun getListCursos()
        fun onStop()
    }
    // Criar o data em outro arquivo, no package data em presenter
}
