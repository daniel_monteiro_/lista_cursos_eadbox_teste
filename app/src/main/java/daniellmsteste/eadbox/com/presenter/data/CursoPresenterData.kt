package daniellmsteste.eadbox.com.presenter.data

import daniellmsteste.eadbox.com.network.RetrofitInitApi
import daniellmsteste.eadbox.com.presenter.view.CursoPresenterView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.annotations.NonNull
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

/**
 * @author Daniel Monteiro
 *
 * @since on 10/07/2018.
 */

class CursoPresenterData: CursoPresenterView.CursosPresenter {

    var viewListaCursos: CursoPresenterView.ViewListaCursos? = null

    @NonNull
    var disposable: Disposable? = null

    constructor(viewListaCursos: CursoPresenterView.ViewListaCursos) {
        this.viewListaCursos = viewListaCursos
    }

    override fun getListCursos() {
        viewListaCursos!!.showProgressLoading()

        if (viewListaCursos!!.checkInternet()) {
            disposable = RetrofitInitApi.instance.getListCursos()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({ listResponse ->
                        viewListaCursos!!.hideProgressLoading()

                        val responseCode = listResponse.code()

                        when(responseCode) {
                            200, 201,202 -> { viewListaCursos!!.onSuccess(listResponse) }
                        }
                    }, { error ->
                        viewListaCursos!!.hideProgressLoading()

                        viewListaCursos!!.onError(error)
                    })
        }else {
            viewListaCursos!!.hideProgressLoading()
            viewListaCursos!!.validateError()
        }
    }

    override fun onStop() {
        if (disposable != null) {
            disposable!!.dispose()
        }
    }
}