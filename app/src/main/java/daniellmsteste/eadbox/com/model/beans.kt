package daniellmsteste.eadbox.com.model

/**
 * @author Daniel Monteiro
 *
 * @since on 09/07/2018.
 */

data class ResponseCursos(
        var course_id: String,
        var course_slug: String, var title: String,
        var description: String?, var is_paid: Boolean, var price: Double,
        var objective: String, var certification: String, var target_audience: String,
        var workload: Int, var time_available: String, var logo_url: String?,
        var category: Categoria?) {

    inner class Categoria(var category_id: String, var category_slug: String, var title: String)

}
