package daniellmsteste.eadbox.com.view.adapters

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.squareup.picasso.Picasso
import daniellmsteste.eadbox.com.R
import daniellmsteste.eadbox.com.model.ResponseCursos

import kotlinx.android.synthetic.main.item_list_curso.view.*

/**
 * @author Daniel Monteiro
 *
 * @since on 09/07/2018.
 */

class AdapterListCursos(private var activity: Activity, var items: ArrayList<ResponseCursos>, var haveNet: Boolean): BaseAdapter() {

    @SuppressLint("ViewHolder", "InflateParams")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view: View?
        val inflater = activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

        view = inflater.inflate(R.layout.item_list_curso, null)

        val curso = items[position]
        if (haveNet)
            Picasso.get().load(curso.logo_url).into(view.civLogoCurso)
        else
            view.civLogoCurso.setBackgroundResource(R.drawable.fail_reload_image)

        view.tvNomeCurso.text = curso.title

        return view as View
    }

    override fun getItem(position: Int): Any {
        return this.items[position]
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getCount(): Int {
        return this.items.count()
    }


}
