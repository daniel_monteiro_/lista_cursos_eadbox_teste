package daniellmsteste.eadbox.com.view.activities

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.view.Menu
import android.view.MenuItem
import android.view.View
import daniellmsteste.eadbox.com.R
import daniellmsteste.eadbox.com.model.ResponseCursos
import daniellmsteste.eadbox.com.network.ConexaoInternet
import daniellmsteste.eadbox.com.persist.Curso
import daniellmsteste.eadbox.com.persist.PersistCursos
import daniellmsteste.eadbox.com.presenter.data.CursoPresenterData
import daniellmsteste.eadbox.com.presenter.view.CursoPresenterView
import daniellmsteste.eadbox.com.view.adapters.AdapterListCursos

import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Response

class MainActivity : AppCompatActivity(), CursoPresenterView.ViewListaCursos {

    var cursoPresenterData: CursoPresenterData? = null
    var adapterListaCursos: AdapterListCursos? = null
    var list: ArrayList<ResponseCursos>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        this.loadCursos()

        lvCursos.setOnItemClickListener { parent, view, position, id ->
            val curso: ResponseCursos = parent.getItemAtPosition(position) as ResponseCursos

            val i = Intent(this, DetalheCursoActivity::class.java)
            i.putExtra("idCurso", curso.course_id)
            i.putExtra("temNet", this.checkInternet())

            startActivity(i)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.list_courses_menu, menu)

        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        when(item?.itemId) {
            R.id.item_menu_reload_courses -> this.loadCursos()
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onDestroy() {
        super.onDestroy()
        if (cursoPresenterData != null)
            cursoPresenterData!!.onStop()
    }

    override fun validateError() {
        this.loadCursosOffline()
    }

    override fun showProgressLoading() {
        pbLoading.visibility = View.VISIBLE
        lvCursos.visibility = View.GONE
    }

    override fun hideProgressLoading() {
        pbLoading.visibility = View.GONE
        lvCursos.visibility = View.VISIBLE
    }

    override fun onSuccess(responseCursos: Response<ArrayList<ResponseCursos>>) {
        this.list = responseCursos.body()!!
        this.adapterListaCursos = AdapterListCursos(this, this.list!!, this.checkInternet())

        lvCursos.adapter = this.adapterListaCursos
        saveLocal()
    }

    override fun onError(throwable: Throwable) {
        this.showMessage(R.string.msg_erro_padrao)
    }

    override fun checkInternet(): Boolean {
        return ConexaoInternet.isConexaoInternet(applicationContext)
    }

    fun loadCursos() {
        if (this.checkInternet()) {
            cursoPresenterData = CursoPresenterData(this)
            cursoPresenterData!!.getListCursos()
        }else {
            loadCursosOffline()
        }

    }

    fun saveLocal() {
        for (c in this.list!!) {
            val curso = Curso()
            curso.course_id = c.course_id
            curso.title = c.title
            curso.description = c.description
            curso.logo_url = c.logo_url
            // Pderia ser os demais dados, mas vou usar só esses.

            val pc = PersistCursos()
            pc.saveCurso(curso)
        }
    }

    fun loadCursosOffline() {
        pbLoading.visibility = View.GONE
        lvCursos.visibility = View.VISIBLE

        val pc = PersistCursos()
        val cursos = pc.getAllCourses()

        val listaCursos = ArrayList<ResponseCursos>()
        for (curso in cursos) {
            val c = ResponseCursos(
                    curso.course_id!!, "", curso.title!!, curso.description,
                    false, 0.0, "", "", "",
                    0, "", curso.logo_url,
                    null)
            listaCursos.add(c)

        }

        this.adapterListaCursos = AdapterListCursos(this, listaCursos, this.checkInternet())

        lvCursos.adapter = this.adapterListaCursos

        if (listaCursos.size == 0) {
            this.showMessage(R.string.msg_nenhum_curso_carregado)
        }
    }

    fun showMessage(msg: Int) {
        val builder = AlertDialog.Builder(this@MainActivity)
        val dialog: AlertDialog?

        builder.setTitle("")

        builder.setMessage(msg)

        builder.setPositiveButton("OK"){ dialog, which ->
            dialog.dismiss()
        }

        dialog = builder.create()
        dialog.show()
    }
}
