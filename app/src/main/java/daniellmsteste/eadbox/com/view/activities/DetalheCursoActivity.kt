package daniellmsteste.eadbox.com.view.activities

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import com.squareup.picasso.Picasso
import daniellmsteste.eadbox.com.R
import daniellmsteste.eadbox.com.persist.Curso
import daniellmsteste.eadbox.com.persist.PersistCursos

import kotlinx.android.synthetic.main.activity_detalhe_curso.*


/**
 * @author Daniel Monteiro
 *
 * @since on 11/07/2018.
 */

class DetalheCursoActivity: AppCompatActivity() {

    private lateinit var bundle: Bundle
    private val persistCurso = PersistCursos()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detalhe_curso)

        this.bundle = intent.extras
        preencherTela()

        supportActionBar?.title = resources.getString(R.string.title_detail_activity_course)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    fun preencherTela() {
        val cursoSelecionado = getCursoSelecionado()
        val temInternet = this.bundle.get("temNet") as Boolean

        if (temInternet)
            Picasso.get().load(cursoSelecionado.logo_url).into(ivLogoCurso)
        else
            ivLogoCurso.setBackgroundResource(R.drawable.fail_reload_image)

        tvNomeCurso.text = cursoSelecionado.title

        if (cursoSelecionado.description != null && cursoSelecionado.description!!.trim() != "")
            tvDescricaoCurso.text = cursoSelecionado.description!!.replace("<br>", "\\\n").replace("\\", "")
        else
            tvDescricaoCurso.text = resources.getString(R.string.label_curso_sem_descricao)

    }

    fun getCursoSelecionado(): Curso {
        val idCursoSelecionado = this.bundle.get("idCurso") as String
        return this.persistCurso.findCourseById(idCursoSelecionado)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item?.itemId) {
            android.R.id.home -> finish()
            else -> super.onOptionsItemSelected(item)

        }

        return super.onOptionsItemSelected(item)
    }

}