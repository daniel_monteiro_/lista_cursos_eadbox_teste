package daniellmsteste.eadbox.com.persist

import com.vicpin.krealmextensions.query
import com.vicpin.krealmextensions.save

/**
 * @author Daniel Monteiro
 *
 * @since on 10/07/2018.
 */

class PersistCursos {

    fun saveCurso(c: Curso) {
        c.save()
    }

    fun findCourseById(id: String): Curso {
        val curso = Curso().query { realmQuery -> realmQuery.equalTo("course_id", id) }
        return curso[0]
    }

    fun getAllCourses(): List<Curso> {
        val cursos = Curso().query { realmQuery -> realmQuery.findAll() }
        return cursos
    }

}