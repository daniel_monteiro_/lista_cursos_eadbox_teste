package daniellmsteste.eadbox.com.persist

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

/**
 * @author Daniel Monteiro
 *
 * @since on 11/07/2018.
 */


open class Curso(
        @PrimaryKey open var course_id: String? = null,
        var course_slug: String? = null, var title: String? = null,
        var description: String? = null, var is_paid: Boolean? = null, var price: Double? = 0.0,
        var objective: String? = null, var certification: String? = null,
        var target_audience: String? = null, var workload: Int? = 0, var time_available: String? = null,
        var logo_url: String? = null, var category: Categoria? = null): RealmObject() {}

open class Categoria(
        @PrimaryKey open var category_id: String? = null,
        var category_slug: String? = null,
        var title: String? = null): RealmObject(){}