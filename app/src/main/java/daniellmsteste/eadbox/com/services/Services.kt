package daniellmsteste.eadbox.com.services

import daniellmsteste.eadbox.com.model.ResponseCursos
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.GET

/**
 * @author Daniel Monteiro
 *
 * @since on 09/07/2018.
 */

interface Services {

    @GET("/api/courses")
    fun getListCursos(): Observable<Response<ArrayList<ResponseCursos>>>
}
