package daniellmsteste.eadbox.com.network

import android.content.Context
import android.net.ConnectivityManager

/**
 * @author Daniel Monteiro
 *
 * @since on 09/07/2018.
 */

class ConexaoInternet {

    companion object {
        fun isConexaoInternet(context: Context): Boolean {
            var connectionManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val activeNetworkInfo = connectionManager.activeNetworkInfo

            return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting
        }
    }

}
