package daniellmsteste.eadbox.com.network

import com.google.gson.GsonBuilder
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import daniellmsteste.eadbox.com.model.ResponseCursos
import daniellmsteste.eadbox.com.services.Services
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * @author Daniel Monteiro
 *
 * @since on 09/07/2018.
 */

class RetrofitInitApi {

    private val services: Services

    init {
        val gson = GsonBuilder()
                .setLenient()
                .create()

        val clientBuilder = OkHttpClient.Builder()
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
        clientBuilder.addInterceptor(loggingInterceptor)

        val retrofit = Retrofit.Builder().baseUrl(BASE_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()

        services = retrofit.create(Services::class.java)
    }

    companion object {
        private val BASE_URL = "https://daniellmsteste.eadbox.com"
        private var apiInit: RetrofitInitApi? = null

        val instance: RetrofitInitApi get() {
            if (apiInit == null) {
                apiInit = RetrofitInitApi()
            }

            return apiInit as RetrofitInitApi
        }
    }

    fun getListCursos(): Observable<Response<ArrayList<ResponseCursos>>> {
        return services.getListCursos()
    }
}
